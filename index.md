---
title: Home
nav_order: 1
description: "Courseware as Code workshop with GitLab"
permalink: /
---

# Courseware as Code Workshop

Welcome to the GitLab Courseware as Code Workshop. This workshop will introduce participants to the basics of Courseware as Code and demonstrate how to set up a Courseware as Code repository in [GitLab.com](https://gitlab.com/).

# Workshop Outline
1. Introduction to Courseware as Code concepts
2. Create a free GitLab.com account
3. Set up the Courseware as Code repository by forking the template
4. Add basic content to the repository
5. Set up the Courseware as Code Tools repository

[Next Page](https://devops-education.gitlab.io/cwac-workshop/course/Introduction/)
