---
title: Set Up Courseware as Code
nav_order: 3
has_children: true
---
# Courseware as Code site template

This repository uses a template for **Courseware as Code** which was designed by Alejandro Rusi during his time with GitLab as a Google Summer of Code intern. It is based on work done by the US Army Cyber School and is Open Source and available for use by anyone. With it, you can generate a static website that can be deployed to Gitlab Pages for free.
Its main objective is to make hosting educational content as easy as possible.

This project started out as a fork of the amazing Jekyll template <a href="https://pmarsceill.github.io/just-the-docs/" target="_blank">just-the-docs</a>.
Courseware as Code wouldn't be possible without it.


[Next Page](https://devops-education.gitlab.io/cwac-workshop/course/fork/)
