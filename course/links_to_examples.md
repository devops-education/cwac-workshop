---
title: Links to Examples
nav_order: 7
parent: Add Content to the Repository
---

# Links to examples

If you want some inspiration and references to make your course-creation process easier, check out some of our example courses created with **Courseware as Code**!
All of these are *Open Source*, so you can check out their source code and the resulting course!

- This very same tutorial!
    - <a href="https://gitlab.com/devops-education/cwac-workshop" target="_blank">Gitlab repository</a>
    - <a href="https://devops-education.gitlab.io/cwac-workshop/" target="_blank">Page</a>

- Git Example used in the tutorial
    - <a href="https://gitlab.com/devops-education/pj-cwac-git-example" target="_blank">Gitlab repository</a>
    - <a href="https://devops-education.gitlab.io/pj-cwac-git-example/" target="_blank">Page</a>
